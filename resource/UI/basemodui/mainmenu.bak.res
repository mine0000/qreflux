"Resource/UI/MainMenu.res"
{
	"MainMenu"
	{
		"ControlName"					"Frame"
		"fieldName"						"MainMenu"
		"xpos"							"0"
		"ypos"							"0"
		"wide"							"f0"
		"tall"							"f0"
		"autoResize"					"0"
		"pinCorner"						"0"
		"visible"						"1"
		"enabled"						"1"
		"tabPosition"					"0"
		"PaintBackgroundType"			"0"
	}
	
	//Temp Logo 
	"Logo"
	{
		"ControlName"					"ImagePanel"
		"fieldName"						"Logo"
		"xpos"							"20"
		"ypos"							"100"	
		"zpos"							"10"		
		"wide"							"512"
		"tall"							"128"
		"visible"						"1"
		"enabled"						"1"
		"image"							"../vgui/logo/logo"
		"alpha"							"255"
		"scaleImage"					"1"	
	}
						
	// Single player
	"BtnPlaySolo"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"						"BtnPlaySolo"
		"xpos"							"88"		[$WIN32WIDE]
		"xpos"							"63"		[!$WIN32WIDE]	
		"ypos"							"258"		
		"wide"							"220"
		"tall"							"20"
		"autoResize"					"1"
		"pinCorner"						"0"
		"visible"						"1"
		"enabled"						"1"
		"tabPosition"					"0"
		"navUp"							"BtnQuit"	
		"navDown"						"BtnOptions"
		//"labelText"					"#PORTALMP_MainMenu_Solo"
		"labelText"						"#GameUI_SingleplayerDialog"
		//"tooltiptext"					"#PORTALMP_MainMenu_PlaySolo_Tip"
		"style"							"MainMenuButton"
		"command"						"SoloPlay"
		"ActivationType"				"1"
		"FocusDisabledBorderSize"		"1"
	}	
	
	/*
	"BtnCoOp"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"						"BtnCoOp"
		"xpos"							"88"	[$WIN32WIDE]
		"xpos"							"63"	[!$WIN32WIDE]	 
		"ypos"							"288"
		"wide"							"220"	
		"tall"							"20"
		"autoResize"					"1"
		"pinCorner"						"0"
		"visible"						"1"
		"enabled"						"1"
		"tabPosition"					"0"
		"navUp"							"BtnPlaySolo"
		"navDown"						"BtnOptions"
		"labelText"						"#PORTALMP_MainMenu_CoOp"
		"tooltiptext"					"#PORTALMP_MainMenu_CoOp_Tip"
		"style"							"MainMenuButton"
		"command"						"MultiPlay"
		"ActivationType"				"1"
	}
	*/
	
	"BtnOptions"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"						"BtnOptions"
		"xpos"							"88"	[$WIN32WIDE]
		"xpos"							"63"	[!$WIN32WIDE]	
		"ypos"							"318"	 
		"wide"							"220"
		"tall"							"20"
		"autoResize"					"1"
		"pinCorner"						"0"
		"visible"						"1"
		"enabled"						"1"
		"tabPosition"					"0"
		"navUp"							"BtnPlaySolo"
		"navDown"						"BtnExtras"
		"labelText"						"#PORTALMP_MainMenu_Options"
		"tooltiptext"					"#PORTALMP_MainMenu_Options_Tip"
		"style"							"MainMenuButton"
		"command"						"Options"
		"ActivationType"				"1"
	}
	
	"BtnExtras"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"						"BtnExtras"
		"xpos"							"88"	[$WIN32WIDE]
		"xpos"							"63"	[!$WIN32WIDE]	
		"ypos"							"348"
		"wide"							"220"
		"tall"							"20"
		"autoResize"					"1"
		"pinCorner"						"0"
		"visible"						"1"
		"enabled"						"1"
		"tabPosition"					"0"
		"navUp"							"BtnOptions"
		"navDown"						"BtnQuit"
		"labelText"						"#PORTALMP_MainMenu_Extras"
		"tooltiptext"					"#PORTALMP_MainMenu_Extras_Tip"
		"style"							"MainMenuButton"
		"command"						"Extras"
		"ActivationType"				"1"
	}

	"BtnQuit"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"						"BtnQuit"
		"xpos"							"88"	[$WIN32WIDE]
		"xpos"							"63"	[!$WIN32WIDE]	
		"ypos"							"378"
		"wide"							"220"
		"tall"							"20"
		"autoResize"					"1"
		"pinCorner"						"0"
		"visible"						"1"
		"enabled"						"1"
		"tabPosition"					"0"
		"navUp"							"BtnExtras"
		"navDown"						"BtnPlaySolo"
		"labelText"						"#PORTALMP_MainMenu_Quit"
		"tooltiptext"					"#PORTALMP_MainMenu_Quit_Tip"
		"style"							"MainMenuButton"
		"command"						"QuitGame"
		"ActivationType"				"1"
	}
	
	/*
	"BtnEconUI"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"						"BtnEconUI"
		"xpos"							"97"	[$WIN32WIDE]
		"xpos"							"72"	[!$WIN32WIDE]	
		"ypos"							"420"   
		"wide"							"220"
		"tall"							"20"
		"autoResize"					"1"
		"pinCorner"						"0"
		"visible"						"1"
		"enabled"						"1"
		"tabPosition"					"0"
		"navUp"							"BtnQuit"
		"navDown"						"BtnPlaySolo"
		"labelText"						"Under Construction"
		"style"							"BitmapButton"
		"command"						"EconUI"
		"ActivationType"				"1"
	}
	*/
}
