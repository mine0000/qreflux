"Resource/UI/MainMenu.res"
{
	"MainMenu"
	{
		"ControlName"					"Frame"
		"fieldName"					"MainMenu"
		"xpos"						"0"
		"ypos"						"0"
		"wide"						"f0"
		"tall"						"f0"
		"autoResize"					"0"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"					"0"
		"PaintBackgroundType"			"0"
	}
		
	//TODO: Make this hard coded?	-lun4r
	// No don't - Graham
	"Logo"
	{
		"ControlName"				"ImagePanel"
		"fieldName"					"Logo"
		"xpos"						"70"
		"ypos"						"100"	
		"zpos"						"10"		
		"wide"						"384"
		"tall"						"96"
		"visible"					"1"
		"enabled"					"1"
		"image"						"../logo/game_logo"
		"alpha"						"255"
		"scaleImage"				"1"	
	}	
	
	"PnlBackground"
	{
		"ControlName"					"Panel"
		"fieldName"					"PnlBackground"
		"xpos"						"60"
		"ypos"						"285"
		"zpos"						"-1"
		"wide"						"f575"
		"tall"						"150"
		"visible"					"0"
		"enabled"					"0"
		"fillColor"					"0 0 0 255"
	}

	// 377, 350, 325, 300
	"BtnDeveloperTest"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"					"BtnDeveloperTest"
		"xpos"						"88"		[$WIN32WIDE]
		"xpos"						"63"		[!$WIN32WIDE]	
		"ypos"						"300"
		"wide"						"220"
		"tall"						"20"
		"autoResize"					"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"					"0"
		"navUp"						"BtnQuit"	
		"navDown"					"BtnOptions"
		"labelText"					"Open Console"
		"style"						"MainMenuButton"
		"command"					"toggleconsole"
		"ActivationType"				"1"
		"FocusDisabledBorderSize"		"1"
	}
	
	"BtnPlaySolo"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"					"BtnPlaySolo"
		"xpos"						"88"		[$WIN32WIDE]
		"xpos"						"63"		[!$WIN32WIDE]	
		"ypos"						"350" //325
		"wide"						"220"
		"tall"						"20"
		"autoResize"					"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"					"0"
		"navUp"						"BtnQuit"	
		"navDown"					"BtnOptions"
		"labelText"					"NEW SESSION"
		"style"						"MainMenuButton"
		"command"					"SoloPlay"
		"ActivationType"				"1"
		"FocusDisabledBorderSize"		"1"
	}
	"BtnOptions"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"					"BtnOptions"
		"xpos"						"88"	[$WIN32WIDE]
		"xpos"						"63"	[!$WIN32WIDE]	
		"ypos"						"377" //350
		"wide"						"220"
		"tall"						"20"
		"autoResize"					"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"					"0"
		"navUp"						"BtnLoad"
		"navDown"					"BtnExtras"
		"labelText"					"CONFIGURE SESSION"
		"style"						"MainMenuButton"
		"command"					"Options"
		"ActivationType"				"1"
	}
	"BtnQuit"
	{
		"ControlName"					"BaseModHybridButton"
		"fieldName"					"BtnQuit"
		"xpos"						"88"	[$WIN32WIDE]
		"xpos"						"63"	[!$WIN32WIDE]	
		"ypos"						"402" // 378 377+25
		"wide"						"220"
		"tall"						"20"
		"autoResize"					"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"					"0"
		"navUp"						"BtnExtras"
		"navDown"					"BtnEconUI"
		"labelText"					"EXIT SESSION"
		"style"						"MainMenuButton"
		"command"					"QuitGame"
		"ActivationType"				"1"
	}
}
