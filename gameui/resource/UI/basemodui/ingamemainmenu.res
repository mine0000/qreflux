"Resource/UI/InGameMainMenu.res"
{
	"InGameMainMenu"
	{
		"ControlName"				"Frame"
		"fieldName"					"InGameMainMenu"
		"xpos"						"0"
		"ypos"						"0"
		"wide"						"f0"
		"tall"						"f0"
		"autoResize"				"0"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"				"0"
		"PaintBackgroundType"		"0"
	}
	
	"PnlBackground"
	{
		"ControlName"				"Panel"
		"fieldName"					"PnlBackground"
		"xpos"						"60"
		"ypos"						"145"
		//"ypos"					"0"
		"zpos"						"-1"
		"wide"						"f500"
		"tall"						"260"
		//"wide"	 				"1000"
		//"tall"	 				"480"
		"visible"					"1"
		"enabled"					"1"
		"fillColor"					"0 0 0 255"
	}
			
	"BtnReturnToGame"
	{
		"ControlName"				"BaseModHybridButton"
		"fieldName"					"BtnReturnToGame"
		"xpos"						"100"
		"ypos"						"148"
		"wide"						"250"
		"tall"						"25"
		"autoResize"				"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"				"1"
		"navUp"						"BtnExitToMainMenu"
		"navDown"					"BtnSingleplayer"
		"labelText"					"#L4D360UI_InGameMainMenu_ReturnToGame"
		"tooltiptext"				"#L4D360UI_InGameMainMenu_ReturnToGame_Tip"
		"style"						"MainMenuButton"
		"command"					"ReturnToGame"
		"ActivationType"			"1"
	}

	"BtnSingleplayer"
	{
		"ControlName"				"BaseModHybridButton"
		"fieldName"					"BtnSingleplayer"
		"xpos"						"100"
		"ypos"						"185" // "265" 45
		"wide"						"250"
		"tall"						"25"
		"autoResize"				"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"				"0"
		"navUp"						"BtnReturnToGame"
		"navDown"					"BtnSaveGame"
		"labelText"					"#GameUI_GameMenu_NewGame"
		"tooltiptext"				"#L4D360UI_MainMenu_PlaySolo_Tip"
		"style"						"MainMenuButton"
		"command"					"SoloPlay"	
		"ActivationType"			"1"
	}

	"BtnSaveGame"
	{
		"ControlName"				"BaseModHybridButton"
		"fieldName"					"BtnSaveGame"
		"xpos"						"100"
		"ypos"						"210"
		"wide"						"250"
		"tall"						"25"
		"autoResize"				"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"				"0"
		"navUp"						"BtnSingleplayer"
		"navDown"					"BtnLoadLastGame"
		"labelText"					"#GameUI_GameMenu_SaveGame"
		"tooltiptext"				"#HL2CEUI_SaveGame_ToolTip"
		"style"						"MainMenuButton"
		"command"					"SaveGame"
		"ActivationType"			"1"
	}

	"BtnLoadLastGame"
	{
		"ControlName"				"BaseModHybridButton"
		"fieldName"					"BtnLoadLastGame"
		"xpos"						"100"
		"ypos"						"235"
		"wide"						"250"
		"tall"						"25"
		"autoResize"				"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"				"1"
		"navUp"						"BtnSaveGame"
		"navDown"					"BtnLeaderboard"
		"labelText"					"#GameUI_GameMenu_LoadGame"
		"tooltiptext"				"#HL2CEUI_LoadGame_ToolTip"
		"style"						"MainMenuButton"
		"command"					"LoadLastSave"
		"ActivationType"			"1"
	}

	"BtnOptions"
	{
		"ControlName"				"BaseModHybridButton"
		"fieldName"					"BtnOptions"
		"xpos"						"100"
		"ypos"						"260"
		//"ypos"					"260"
		"wide"						"250"
		"tall"						"25"
		"autoResize"				"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"				"0"
		"navUp"						"BtnStatsAndAchievements"
		"navDown"					"BtnExitToMainMenu"
		"labelText"					"#L4D360UI_MainMenu_Options"
		"tooltiptext"				"#L4D360UI_MainMenu_Options_Tip"
		"style"						"MainMenuButton"
		"command"					"Options"
		"ActivationType"			"1"
	}

	"BtnExitToMainMenu"
	{
		"ControlName"				"BaseModHybridButton"
		"fieldName"					"BtnExitToMainMenu"
		"xpos"						"100"
		"ypos"						"297"
		"wide"						"250"
		"tall"						"25"
		"autoResize"				"1"
		"pinCorner"					"0"
		"visible"					"1"
		"enabled"					"1"
		"tabPosition"				"0"
		"navUp"						"BtnOptions"
		"navDown"					"BtnReturnToGame"
		"labelText"					"#L4D360UI_InGameMainMenu_ExitToMainMenu"
		"tooltiptext"				"#L4D360UI_InGameMainMenu_ExitToMainMenu_Tip"
		"style"						"MainMenuButton"
		"command"					"ExitToMainMenu"
		"ActivationType"			"1"
	}
}
