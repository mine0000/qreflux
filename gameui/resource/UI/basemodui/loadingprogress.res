"Resource/UI/Downloads.res"
{
	"LoadingProgress"
	{	
		"ControlName"			"Frame"
		"fieldName"				"LoadingProgress"
		"xpos"					"0"
		"ypos"					"0"
		"wide"					"f0"
		"tall"					"f0"
		"autoResize"			"0"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
	}

	"ProTotalProgress"
	{
		"ControlName"				"ContinuousProgressBar"
		"fieldName"				"ProTotalProgress"
		"xpos"					"r158" [$WIN32]
		"xpos"					"r158" [$X360HIDEF]
		"xpos"					"r140" [$X360LODEF]
		"ypos"					"r45"
		"wide"					"135" [$WIN32]
		"wide"					"135" [$X360HIDEF]
		"wide"					"120" [$X360LODEF]
		"tall"					"33" [$WIN32]
		"tall"					"33" [$X360HIDEF]	// texture is 4:1 w:h ratio
		"tall"					"30" [$X360LODEF]
		"zpos"					"5"
		"autoResize"				"0"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"				"0"
		"usetitlesafe"			"1"
	}
	
	"WorkingAnim"
	{
		"ControlName"				"ImagePanel"
		"fieldName"				"WorkingAnim"
		"xpos"					"0"
		"ypos"					"0"
		"zpos"					"50"
		"wide"					"40"
		"tall"					"40"
		"visible"				"0"
		"enabled"				"1"
		"tabPosition"				"0"
		"scaleImage"				"1"
		"image"					"spinner"
		"frame"					"0"
	}
	
	"BGImage"
	{
		"ControlName"		"ImagePanel"
		"fieldName"			"BGImage"
		"xpos"				"0"
		"ypos"				"0"
		"wide"				"f0"
		"tall"				"f0"
		"zpos"				"2"
		"scaleImage"		"1"
		"visible"			"0"
		"enabled"			"1"
	}
	
	"Poster"
	{
		"ControlName"		"ImagePanel"
		"fieldName"			"Poster"
		"xpos"				"c-240"
		"ypos"				"0"
		"wide"				"480"
		"tall"				"f0"
		"zpos"				"3"
		"scaleImage"		"1"
		"visible"			"0"
		"enabled"			"1"
		"image"				""
	}

	"PlayerNames"
	{
		"ControlName"				"Label"
		"fieldName"				"PlayerNames"
		"xpos"					"0"
		"ypos"					"0"
		"zpos"					"5"
		"wide"					"475" [$WIN32]
		"wide"					"475" [$X360HIDEF]
		"wide"					"350" [$X360LODEF]
		"tall"					"32"
		"wrap"					"1"
		"autoResize"				"1"
		"pinCorner"				"0"
		"visible"				"0"
		"enabled"				"1"
		"tabPosition"				"0"
		"Font"					"DefaultMedium"
		"textAlignment"				"north-west"
		"labelText"				""
		"noshortcutsyntax"			"1"

		"pin_to_sibling"			"StarringLabel"
		"pin_corner_to_sibling"			"0"	
		"pin_to_sibling_corner"			"1"	
	}	
}